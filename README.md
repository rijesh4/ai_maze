AI Maze Generator 			
TECHNICAL MANUAL: 

ACTIVITIES:
1.	MainActivity:
a.	Displays the Welcome screen
b.	Asks the user to input the size of the maze to start generating maze.
c.	Contains a button submit, which initiates a new intent.
d.	Checks for the error in input. 
e.	Contains the LOAD button to load a maze from the file. 

2.	The_Maze:
a.	Creates a new View class which in turn does the following:
i.	The onDraw() method creates uses the width and Height of the screen to generate maze, a cell at a time. 
ii.	Modifies the VisitedCells, using update() method, that adds children to the visitedCells
iii.	There is a time delay to mimic animation of creation the maze. 
iv.	If the maze is incomplete the activity toasts the message and brings back to the previous screen to initiate again. 



b.	Creates a View Class PathView:
i.	Using the selected menu Item for the Searches and the file, it creates the screen
ii.	It is responsible for the update of the screen to incorporate the animation of the path being traversed.
iii.	Responsible for calling the object of the MazeTree to according to the path being traversed by the algorithm
JAVA CLASSES:
1.	CREATEMAZE.JAVA
a.	The class creates a list of cells based on the size of the maze.
b.	It creates a list of Cell and initiates its value. 
c.	The cell update() method, updates the isWay and isVisited, based on the AI Algorithm given below. 
d.	It has a list of private members and getters and setters to support use of those variables by other classes. 
2.	CELL.JAVA
a.	Each cell has the isVisited (boolean), and isWay[list] members, which can be used by the onDraw method in view to draw the Cell
b.	It has respective getters and setters. 
3.	MazeTree.JAVA
a.	The class creates a tree from any given maze.
b.	It finds the headNode and traverses it according to its valid children
c.	It is responsible for using branch and bound algorithm to find the complexity of the maze as well as for individual cells
d.	It is responsible for finding the depth and heuristic values of each cell
e.	It is responsible for finding the traversal path for Blind as well as Heuristic searches namely : Depth First Search, Breadth First Search, A* Search, Best First Search 
AI MAZE GENERATION ALGORITHM
1.	Start with a random cell with xPos = 0. 
2.	Place the cell in the visitedCell list. 
3.	Draw the cell until the visitedCell list is empty
4.	Get its list of children, based on computer isWay.
a.	Compute the random value for isWay based on the number of children. {1/rand(numOfChildren) + 1}
b.	If the isWay value is 0 -> decrease the number of children to increase the probability of going to next Cell.  
5.	Update the cell and visitedCell. Append children to Visited cell 
6.	Repeat until the Cell has no more children. 

AI Path Finding Algorithm:
1.	Create a tree using the given file
2.	Use stack for holding active Depth First Search List
3.	Use List for holding active Breadth first Search List, as well as A* and Best First
4.	The following algorithm is used for the specified Searches:
a.	Depth First Search:
i.	Push head Node onto Stack
ii.	 In a loop, remove from stack and push its children
iii.	If goal found stop.
b.	Breadth First Search:
i.	Push head Node onto List
ii.	 In a loop, remove from stack and push its children
iii.	If goal found stop.

Heuristic Search Algorithm:
1.	The heuristic searches uses the following mechanisms:
a.	Best First Search : 
i.	Average of Manhattan distance from the exit nodes
b.	A* Search:
i.	Average of Manhattan distance from the exit node, and
ii.	Depth of the cell
2.	Keep a track of active List of Cells. 
3.	Remove the current Node from the list and add its children
4.	Sort the List and continue until goal node found. 
BUG REPORT:
The program seems to be bug free, as of now. 
FEATURE REPORT:
	The program has a fluid animation, which is aesthetic to common eyes, but may be a hindrance to understand the mechanism behind the algorithm being followed. To understand the heuristic values/ depth being used for the Heuristic searches, one may have to follow the console log for the output. 
