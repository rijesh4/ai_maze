package edu.ramapo.rk.ai_maze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

public class PathView extends View {
	
	private int mazeSize; 
	private MazeTree curMaze; 
	int testCounter = 1; 
	private int menuCode; 
	private ArrayList<Cell> BFS_Path = new ArrayList<Cell>(); 
	private ArrayList<Cell> activeList = new ArrayList<Cell>();
	Stack<Cell> DFS_stack = new Stack<Cell>();
	private Cell DFS_node = new Cell(-1,-1);
	private int curNodeCounter = 0 ; //number of elements in the curList
	private boolean doPause = false; 
	private Cell chosenExitCell = new Cell (-1, -1) ; 
	private int ACounter = 0 ; 
	//boolean exitFound = false; 
	
	/** 
	 * Function Name: PathView
	   Purpose: Initialize the view for the maze creation and path traversal  
       Parameters:  context, mazeSize, menuCode : menuItem selected : pathCode -> file Selected
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Create a new CreateMaze object passing in the mazeSize
	2) Initialize the object members.  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public PathView(Context context, int mazeSize, int menuCode , int pathCode ){
		super(context);
		
		initMazeCells(pathCode); 
		curMaze.createTree();
		this.menuCode = menuCode; 
		BFS_Path.add(curMaze.retHeadNode());
		DFS_stack.push(curMaze.retHeadNode()); 
		DFS_node = curMaze.retHeadNode();
		if (menuCode == 5) {
			activeList = curMaze.AstarSearch(false); 
		}
		else if (menuCode == 4) {
			activeList= curMaze.AstarSearch(true);
		}
		
	}
	
	/** 
	 * Function Name: onDraw()
	   Purpose: Draws the elements of a cube to the screen to create a maze 
	   Parameters:  Canvas canvas
	   Return Value: void
	   Local Variables: Paint paint -> paint brush to draw
	   					cellWidth/cellHeight 	-> size of each cell 
	   					tempCount -> counter for the current traversing cell
	   					int duration -> toast timer
	   		
	Algorithm: 
	1) Give the menuCode and pathCode decides which maze to draw and which algorithm to use
	2) Check if the maze is not complete:
		call update() function. 
		and then call Invalidate() to redraw.
	3) BFS and DFS : 
		i) pops elements from respective stack/List -> draw and update()/ Invalidate
	4) Astar/BestFirst : 
		i) calls the Astar function from the MazeTree object Class
		ii) uses the returned List to draw-> update()/Invalidate(); 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	
	 @Override
	protected void onDraw(Canvas canvas) {
		 super.onDraw(canvas);
		 Paint paint = new Paint();
	     paint.setStyle(Paint.Style.STROKE);
	     paint.setStrokeWidth(20);
	     paint.setColor(Color.WHITE);
	     paint.setColor(Color.parseColor("#CD5C5C"));  
	     
	     curMaze.setCellSize(getWidth(), getHeight()); 
	     float cellWidth = curMaze.getCellWidth();
	     float cellHeight = curMaze.getCellHeight();
	     int count = 0; 
	     Cell listCell[] = curMaze.retCells();
	     
	     
	     
	     while(count < mazeSize * mazeSize) {
	    	 Cell aCell = listCell[count]; 
	    	 
	    	 	//left
	    	   if (aCell.isWay[0] == 0) {
	    		   canvas.drawLine(aCell.xPos * cellWidth , aCell.yPos * cellHeight , aCell.xPos * cellWidth ,(aCell.yPos + 1 )* cellHeight , paint); 
	    	   }
	    	   //right
	    	   if (aCell.isWay[1] == 0) {
	    		   canvas.drawLine((aCell.xPos+1) * cellWidth , aCell.yPos * cellHeight , (aCell.xPos+1) * cellWidth ,(aCell.yPos +1) * cellHeight , paint); 
	    	   }
	    	   //top
	    	   if (aCell.isWay[2] == 0) {
	    		   canvas.drawLine(aCell.xPos * cellWidth , aCell.yPos * cellHeight , (aCell.xPos+1) * cellWidth ,aCell.yPos * cellHeight , paint); 
	    	   }
	    	   //bottom
	    	   if (aCell.isWay[3] == 0) {
	    		   canvas.drawLine(aCell.xPos * cellWidth , (aCell.yPos+1) * cellHeight , (aCell.xPos+1) * cellWidth ,(aCell.yPos+1) * cellHeight , paint); 
	    	   }
	    	   count++; 
	     }
	     
	     if (menuCode == 3 ) {
	    	 float complexity = curMaze.branchAndBound()/mazeSize;
	    	 String text = "The Complexity of the maze is : ";
	    	 text += Float.toString(complexity);
	    	 int duration = 5000;
    		 Toast toast = Toast.makeText(getContext(), text, duration);
			 toast.show();
			 doPause = true; 
	     }
	     
	     if (doPause && (menuCode != 3 )) {
	    	 paint.setColor(Color.BLACK);
	    	 Cell cCell = new Cell(-1,-1);
    		 ArrayList<Cell>pathFromNode = curMaze.getPathtoStart(chosenExitCell); 
             while (pathFromNode.size() > 1 ) {
                 cCell = pathFromNode.remove(1);
                 canvas.drawLine((chosenExitCell.xPos + 0.5f) * cellWidth , (chosenExitCell.yPos+0.5f ) * cellHeight , (cCell.xPos+0.5f) * cellWidth ,(cCell.yPos+0.5f) * cellHeight , paint);
                 chosenExitCell = cCell; 
            }
	     }
	     
	     if (menuCode == 1 && !doPause) {
	    	 doPause = false; 
	    	 paint.setColor(Color.BLACK);	   	 
	    	 
	    	 int tempCount = 0; 
	    	 while (tempCount <= curNodeCounter){
	    		 Cell bCell = BFS_Path.get(tempCount++);
	    		 for (int i = 0 ; i < curMaze.retExitCells().size(); i++) {
	    			 if (curMaze.retExitCells().get(i).xPos == bCell.xPos && curMaze.retExitCells().get(i).yPos == bCell.yPos ) {
	    				 doPause = true; 
	    				 chosenExitCell = bCell;
	    				 invalidate();
	    			 }
	    		 }
	    		 
	    		 //System.out.println(bCell.xPos + " " + bCell.yPos );
	    	 
	    		 Cell cCell = new Cell(-1,-1);
	    		 ArrayList<Cell>pathFromNode = curMaze.getPathtoStart(bCell); 
	             while (pathFromNode.size() > 1 ) {
	                 cCell = pathFromNode.remove(1);
	                 canvas.drawLine((bCell.xPos + 0.5f) * cellWidth , (bCell.yPos+0.5f ) * cellHeight , (cCell.xPos+0.5f) * cellWidth ,(cCell.yPos+0.5f) * cellHeight , paint);
	                 bCell = cCell; 
	            }
	    	} 
	     }
	     
	     if (menuCode == 2 && !doPause) {
	    	 doPause = false; 
	    	 paint.setColor(Color.BLACK);
	    	 
	    	 Cell bCell = DFS_node; 
	    	 Cell cCell = new Cell(-1,-1);
	    	 for (int i = 0 ; i < curMaze.retExitCells().size(); i++) {
    			 if (curMaze.retExitCells().get(i).xPos == bCell.xPos && curMaze.retExitCells().get(i).yPos == bCell.yPos ) {
    				 doPause = true; 
    			 }
    		 }
    		 ArrayList<Cell>pathFromNode = curMaze.getPathtoStart(bCell); 
             while (pathFromNode.size() > 1) {
                 cCell = pathFromNode.remove(1);
                 canvas.drawLine((bCell.xPos + 0.5f) * cellWidth , (bCell.yPos+0.5f ) * cellHeight , (cCell.xPos+0.5f) * cellWidth ,(cCell.yPos+0.5f) * cellHeight , paint);
                 bCell = cCell; 
            }
	    	 
	     }
	     
	     if ((menuCode == 4 || menuCode == 5 )&& !doPause) {
	    	
	    	 if (ACounter >= activeList.size() -1 ) { 
				 ACounter -- ; 
    			 doPause  = true;
    			 invalidate();
			 }
	    	 String text = "The Heuristic Val is: ";
	    	 if (menuCode == 4) {
	    		 text += Float.toString(activeList.get(ACounter).heuristicVal );
	    	 }
	    	 if (menuCode == 5) {
	    		 text += Float.toString(activeList.get(ACounter).heuristicVal);
	    		 text += " "; 
	    		 text += Float.toString(activeList.get(ACounter).depth); 
	    		 
	    	 }
	    	
	    	 for (int i = 0 ; i <= ACounter ; i++ ) {
	    		 
	    		 Cell bCell = activeList.get(i); 
	    		 if (bCell.xPos == mazeSize -1 && bCell.isWay[1] == 1) {
	    			 chosenExitCell = bCell; 
	    			 doPause = true; 
	    			// exitFound = true; 
	    		 }
	    		 
	    		 paint.setColor(Color.BLACK); 
	    		 paint.setStyle(Paint.Style.FILL);
	    		 float radius = curMaze.getCellWidth()/8; 
	    		 canvas.drawCircle((bCell.xPos + 0.5f) * cellWidth, (bCell.yPos+0.5f ) * cellHeight, radius, paint);
	    	 }   	 
	     }
	        
	     if (!doPause) {
	    	 update(); 
		     
		     try {  
		          Thread.sleep(500);  
		       } catch (InterruptedException e) { }
		     
		     invalidate();
	     }
	    	      
	     
	}//End of OnDraw	 
	
		/** 
		 * Function Name: update()
		   Purpose: Update the list of VistedCell, and init redraw of screen.  
		   Parameters:  NONE
		   Return Value: void
		   Local Variables: tempCount -> Stores the current number of elements in the VisitedCell
		   					childTempCount -> Stores current num of Child of the parent(curCell)
		   					tempCount -> counter for the current traversing cell
		   					temp -> a variation of loop counter that makes sure to be updated when to get the current position of the curCell
		   		
		Algorithm: 
		1) Get the curCell
		2) Find the list of its children.
		3) If not visited -> add to the visitedCell list. 
		4) Modify ChildCounter, and curCell 
		5) repeat for other searches except for menuCode 4,5 where increment Acounter
		 
		Assistance Received: NONE 
		 * 
		 * 
		   @param 
		   @return 
		*/  
	 private void update(){
		 if (menuCode == 5 || menuCode == 4) {
			 ACounter ++; 
		 }
		 
		if (menuCode == 2) {
			Cell curNode = new Cell(-1,-1);
	    	 if (!DFS_stack.isEmpty()) {
	    		 curNode = DFS_stack.pop();
	    		 DFS_node = curNode; 
	    		 if (curNode.getLChild().xPos != -1) { 
	    			 DFS_stack.push(curNode.getLChild());
	 			 }
	 			 if (curNode.getRChild().xPos != -1) {
	 				 DFS_stack.push(curNode.getRChild());
	 			 }
	 			 if (curNode.getTChild().xPos != -1) {
	 				 DFS_stack.push(curNode.getTChild());
	 			 }
	 			 if (curNode.getBChild().xPos != -1) {
	 				 DFS_stack.push(curNode.getBChild());
	 			 }
	    	 }
		} 
		
		if (menuCode == 1) {
			
			int curCount = -1; 
			boolean isDeadEnd = false; 
			while (curCount < BFS_Path.size()-1) {
				isDeadEnd = true; 
				Cell curNode = BFS_Path.remove(0);
				if (curNode.getLChild().xPos != -1) {
					BFS_Path.add(curNode.getLChild());
					curCount++;
					isDeadEnd = false; 
				}
				
				if (curNode.getBChild().xPos != -1) {
					BFS_Path.add(curNode.getBChild());
					curCount++;
					isDeadEnd = false; 
				}
				if (curNode.getTChild().xPos != -1) {
					BFS_Path.add(curNode.getTChild());
					curCount++;
					isDeadEnd = false; 
				}
				if (curNode.getRChild().xPos != -1) {
					BFS_Path.add(curNode.getRChild());
					curCount++;
					isDeadEnd = false; 
				}
				
				if (isDeadEnd) { 
					BFS_Path.remove(curNode); 
				}
			}
			curNodeCounter = curCount; 
			
		}
	 }
	 
	 
		/** 
		 * Function Name: InitMazeCells()
		   Purpose: read the data from the file to create the maze  
		   Parameters:  NONE
		   Return Value: void
		   Local Variables: String: fileName
		Algorithm: 
		1) Read the file one line at a time 
			i) for each line check for space or pipes 
			ii) create walls accordingly
		 
		Assistance Received: NONE 
		 * 
		 * 
		   @param 
		   @return 
		*/  
	private void initMazeCells( int aVal){
		String fileName = "file1.txt"; 
		if (aVal == 0) {
			fileName = "file1.txt";
		}
		else {
			int x = aVal - 5; 
			fileName = "file" + Integer.toString(x) + ".txt"; 
		}
		 
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,fileName);
		//Read text from file
		
		 try {
			 BufferedReader br = new BufferedReader(new FileReader(file));
			 String topLine = "";
			 String midLine = "";
			 String bottomLine = "";
			 topLine = br.readLine(); 
			 char[] topCharArray = topLine.toCharArray(); 
			 char[] midCharArray;
			 char[] bottomCharArray;
			 
			 //Get the size of the maze
			 int mazeCount = 0; 
			 for (int i = 0 ; i < topLine.length() ; i++) {
				 if (topCharArray[i] == ' ' || topCharArray[i] == '-' ) {
					 mazeCount++;  
				 }
			 }
			 // Create a Maze of Size  mazeCount 
			 curMaze = new MazeTree(mazeCount/2);
			 curMaze.reverseInitCells(); 
			 mazeSize = mazeCount/2;  
			
			 
			 midLine = br.readLine(); 
			 //bottomLine = br.readLine(); 
			 int cellNum = 0; 
			 Cell cell;// = curMaze.retCells()[cellNum];
			 
			 while ((bottomLine = br.readLine()) != null ) {
				 
				 topCharArray = topLine.toCharArray();
				 midCharArray = midLine.toCharArray();
				 bottomCharArray = bottomLine.toCharArray();
				 
				 for (int i = 1 ;  i <= mazeSize; i++) {
					 cell = curMaze.retCells()[cellNum];
					 if (topCharArray[i*2 - 1] == ' '  ) {
						 cell.isWay[2] = 1; 
					 } 
					 else {
						 cell.isWay[2] = 0; 
					 }
					 if (midCharArray[i*2 - 2] == ' ') {
						 cell.isWay[0] = 1;
					 }
					 else {
						 cell.isWay[0] = 0;
					 }
					 if (bottomCharArray[i*2 - 1] == ' '  ) {
						 cell.isWay[3] = 1; 
					 } 
					 else {
						 cell.isWay[3] = 0; 
					 }
					 if (midCharArray[i*2] == ' ') {
						 cell.isWay[1] = 1;
					 }
					 else {
						 cell.isWay[1] = 0;
					 }
					 curMaze.setCells(cell, cellNum++); 	
				 }
				 topLine = bottomLine;
				 midLine = br.readLine(); 
				 //midLine = topLine; 
			 }
			 
		 }
		 catch (IOException e) {
			    // proper error handling here
		 }
		
	} 

}


