 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.ai_maze;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.Toast;
//import android.app.DialogFragment;

public class MazeView extends View{
	
	/** 
	 * Function Name: MazeView
	   Purpose: Initialize the data for the maze generation.  
       Parameters:  context, mazeSize
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Create a new CreateMaze object passing in the mazeSize
	2) Initialize the object members.  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public MazeView(Context context, int mazeSize ){
		super(context);
		
		curMazeSize = mazeSize;
		curMaze = new CreateMaze(curMazeSize);
		visitedCells = new Cell[curMazeSize*curMazeSize];
		curMaze.initCells();//
		resetCells(4, childCells);
		resetCells(mazeSize*mazeSize, visitedCells);
		//
		childCells[0] = curMaze.getInitCell();
		//
		curCell = childCells[0];
		curMaze.updateCells(curCell);
		curCell.isWay[0] = 1; 
		visitedCells[count] = curCell;
		entryCell = curCell; 
		curMaze.setCells(curCell, curMaze.getCellNum(curCell)); 
	}
	
	private int curMazeSize;  
	private CreateMaze curMaze;
	private Cell visitedCells[]; 
	private Cell childCells[] = new Cell[4]; 
	private Cell curCell = new Cell(-1,-1);
	private int count = 0; 
	private int childCounter = 1; 
	private boolean isComplete = false; 
	private Cell entryCell = new Cell(-1,-1); 
	boolean createdIncompleteMaze = true;
	
	 
	/** 
	 * Function Name: onDraw()
	   Purpose: Draws the elements of a cube to the screen to create a maze 
	   Parameters:  Canvas canvas
	   Return Value: void
	   Local Variables: Paint paint -> paint brush to draw
	   					cellWidth/cellHeight 	-> size of each cell 
	   					tempCount -> counter for the current traversing cell
	   					int duration -> toast timer
	   		
	Algorithm: 
	1) While visitedCell is not empty :
		draw the cell. 0 -> drawline 1 -> no line
	2) Check if the maze is not complete:
		call update() function. 
		and then call Invalidate() to redraw.
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	
    @Override
    protected void onDraw(Canvas canvas) {
       super.onDraw(canvas);
       
       
       Paint paint = new Paint();
       paint.setStyle(Paint.Style.STROKE);
       paint.setStrokeWidth(20);
       paint.setColor(Color.WHITE);
       paint.setColor(Color.parseColor("#CD5C5C"));
       curMaze.setCellSize(getWidth(), getHeight()); 
       float cellWidth = curMaze.getCellWidth();
       float cellHeight = curMaze.getCellHeight();
       
       int tempCount = 0;
       count = 0;
       while(visitedCells[tempCount].xPos != -1){
    	   Cell aCell = visitedCells[tempCount++];
    	   //left
    	   if (aCell.isWay[0] == 0) {
    		   canvas.drawLine(aCell.xPos * cellWidth , aCell.yPos * cellHeight , aCell.xPos * cellWidth ,(aCell.yPos + 1 )* cellHeight , paint); 
    	   }
    	   //right
    	   if (aCell.isWay[1] == 0) {
    		   canvas.drawLine((aCell.xPos+1) * cellWidth , aCell.yPos * cellHeight , (aCell.xPos+1) * cellWidth ,(aCell.yPos +1) * cellHeight , paint); 
    	   }
    	   //top
    	   if (aCell.isWay[3] == 0) {
    		   canvas.drawLine(aCell.xPos * cellWidth , aCell.yPos * cellHeight , (aCell.xPos+1) * cellWidth ,aCell.yPos * cellHeight , paint); 
    	   }
    	   //bottom
    	   if (aCell.isWay[2] == 0) {
    		   canvas.drawLine(aCell.xPos * cellWidth , (aCell.yPos+1) * cellHeight , (aCell.xPos+1) * cellWidth ,(aCell.yPos+1) * cellHeight , paint); 
    	   }
    	   count++;
    	   
       }
       
       if (!isComplete) {
    	   update();
       }
       else{
    	   paint.setColor(Color.WHITE);
    	   canvas.drawLine(entryCell.xPos * cellWidth , entryCell.yPos * cellHeight , entryCell.xPos * cellWidth ,(entryCell.yPos + 1 )* cellHeight , paint); 
       }
       
       try {  
          Thread.sleep(300);  
       } catch (InterruptedException e) { }
       
       if (!isComplete) {
    	   invalidate();  // Force re-draw 
       }
       else if (createdIncompleteMaze) {
		  
		   CharSequence text = "Incomplete Maze. Please try again. ";
		   int duration = 5000;

		   Toast toast = Toast.makeText(this.getContext(), text, duration);
		   toast.show();   
		   /*try {  
		          Thread.sleep(5000);  
		       } catch (InterruptedException e) { }*/
		   //Context mContext = this;// = new Context(this); 
		   ((Activity)this.getContext()).finish();
		   
	   }
   }
   
	/** 
	 * Function Name: update()
	   Purpose: Update the list of VistedCell, and init redraw of screen.  
	   Parameters:  NONE
	   Return Value: void
	   Local Variables: tempCount -> Stores the current number of elements in the VisitedCell
	   					childTempCount -> Stores current num of Child of the parent(curCell)
	   					tempCount -> counter for the current traversing cell
	   					temp -> a variation of loop counter that makes sure to be updated when to get the current position of the curCell
	   		
	Algorithm: 
	1) Get the curCell
	2) Find the list of its children.
	3) If not visited -> add to the visitedCell list. 
	4) Modify ChildCounter, and curCell 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/ 
   protected void update(){
	   int tempCount = count; 
	   int childTempCount = 0; 
	   int temp = 0;  
	   
	   for (int j = 0 ; j < childCounter; j++) { 
		   curCell = visitedCells[count - (childCounter - temp++)]; 
		   childCells = curMaze.getCellChild(curCell);
		   for (int i = 0 ; i < 4 ; i++ ) {
			   if (childCells[i].xPos == -1) {
				   continue; 
			   }
			   else {
				   if (curCell.isWay[i] == 0 || curMaze.retCell(childCells[i]).isVisited == true ) { 
					   continue;
				   }
				   childTempCount++; 
				   curMaze.updateCells(childCells[i]);
				   visitedCells[tempCount++] = childCells[i];
				   if (!isComplete && childCells[i].xPos == curMazeSize - 1 && createdIncompleteMaze) { //remove the criteria createdIncompleteMaze to have mult. exits.
					   createdIncompleteMaze = false; 
					   //isComplete = true;
					   childCells[i].isWay[1] = 1;
					   //childCells[i].isWay[2] = 0;
					   //childCells[i].isWay[3] = 0;
					   //exitCell = childCells[i]; 
				   }
			   }
			   curMaze.setCells(childCells[i], curMaze.getCellNum(childCells[i]));
		   }
	   }
	   childCounter = childTempCount;
	   if (childCounter == 0) {
		   isComplete = true; 
	   }
   } 
   public void resetCells(int val, Cell c[]){
	   Cell temp = new Cell(-1,-1); 
	   for (int i = 0 ; i <val ; i++  ) {
			c[i] = temp; 
		}
   }
}



