 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.ai_maze;

import java.io.Serializable;

public class Cell implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7294244881360025480L;
	public float xPos;  
	public float yPos;  
	public boolean isVisited; 
	public int isWay[] = new int [4]; 
	public float complexityVal = 0; 
	public int depth = 0; 
	public float heuristicVal = 0; 
	private Cell lChild; 
	private Cell rChild;
	private Cell tChild;
	private Cell bChild;
	private Cell parent; 
	
	/** 
	 * function Name: Cell()
	   Purpose: Default Constructor 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	1) Initialize private member variables
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell(){
		resetIsWay();
		xPos = -1; 
		yPos = -1;
		isVisited = false;
		lChild = new Cell (-1,-1);
		rChild = new Cell (-1,-1);
		tChild = new Cell (-1,-1);
		bChild = new Cell (-1,-1);
		parent = new Cell (-1,-1);
		for (int i = 0 ; i < 4 ; i++) {
			isWay[i] = 0; 
		}
	}
	
	/** 
	 * function Name: Cell(float x, float y)
	   Purpose: Another Constructor 
	   Parameters: float x, float y 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	1) Initialize private member variables using input params
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell(float x, float y){
		xPos = x; 
		yPos = y;
		isVisited = false; 
		resetIsWay();
		for (int i = 0 ; i < 4 ; i++) {
			isWay[i] = 0; 
		}
	}
	
	/** 
	 * function Name: setXPos(float val)
	   Purpose: xPos setter 
	   Parameters: float val
	   Return Value: void
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setXPos(float val) {
		xPos = val; 
	}
	
	/** 
	 * function Name: setYPos(float val)
	   Purpose: yPos setter 
	   Parameters: float val
	   Return Value: void
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setYPos(float val) {
		yPos = val; 
	}
	
	/** 
	 * function Name:resetIsWay ()
	   Purpose: reset the isWay values of a Cell 
	   Parameters: None
	   Return Value: void
	   Local Variables: int i -> loop counter
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void resetIsWay () {
		for (int i = 0 ; i < 4; i ++) {
			isWay[i] = 0; 
		}
	}
	

	/** 
	 * function Name:getLChild
	   Purpose: returns the left child of the Cell 
	   Parameters: None
	   Return Value: Cell
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell getLChild() {
		return lChild; 
	}
	/** 
	 * function Name:getRChild
	   Purpose: returns the right child of the Cell 
	   Parameters: None
	   Return Value: Cell
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell getRChild() {
		return rChild; 
	}
	/** 
	 * function Name:getTChild
	   Purpose: returns the Top child of the Cell 
	   Parameters: None
	   Return Value: Cell
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell getTChild() {
		return tChild; 
	}
	/** 
	 * function Name:getBChild
	   Purpose: returns the bottom child of the Cell 
	   Parameters: None
	   Return Value: Cell
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell getBChild() {
		return bChild; 
	}
	/** 
	 * function Name:getParent
	   Purpose: returns parent of the Cell 
	   Parameters: None
	   Return Value: Cell
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public Cell getParent() {
		return parent; 
	}
	
	/** 
	 * function Name:setLChild
	   Purpose: Set left child of a Cell 
	   Parameters: Cell
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setLChild(Cell aCell){
		lChild = aCell; 
	}
	/** 
	 * function Name:setRChild
	   Purpose: Set right child of a Cell 
	   Parameters: Cell
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setRChild(Cell aCell){
		rChild = aCell; 
	}
	/** 
	 * function Name:setTChild
	   Purpose: Set top child of a Cell 
	   Parameters: Cell
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setTChild(Cell aCell){
		tChild = aCell; 
	}
	/** 
	 * function Name:setBChild
	   Purpose: Set bottom child of a Cell 
	   Parameters: Cell
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setBChild(Cell aCell){
		bChild = aCell; 
	}
	/** 
	 * function Name:setParent
	   Purpose: Set parent of a Cell 
	   Parameters: Cell
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void setParent(Cell aCell){
		parent = aCell; 
	}
	
}
