 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.ai_maze;

import java.io.Serializable;
import java.util.Random;

public class CreateMaze implements Serializable {

	/**
	 * private variables
	 */
	private static final long serialVersionUID = -171791810840093913L;
	protected int mazeSize; 
	protected int cWidth; 
	protected int cHeight;
	protected Cell allCell[];
	//private MazeTree theTree;// = new MazeTree(this); 
	
	/** 
	 * Function Name: CreateMaze(int mazeSize)
	   Purpose: Default Constructor 
       Parameters:  int mazeSize -> Accepts the size of the maze to create. 
	   Return Value: 
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */		
	public CreateMaze(int mazeSize){
		this.mazeSize = mazeSize;
		allCell =  new Cell[mazeSize*mazeSize]; 
	}
	public CreateMaze(){
		//allCell =  new Cell[mazeSize*mazeSize];
	}

	/** 
	 * Function Name: setCellSize(int cWidth, int cHeight)
	   Purpose: Sets the Screen Size based on parameters 
       Parameters:  int cWidth/cHeight -> get the width/height of the screen 
	   Return Value: 
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */		
	public void setCellSize(int cWidth, int cHeight){
		this.cWidth = cWidth; 
		this.cHeight = cHeight; 
	}

	/** 
	 * Function Name: getCellWidth(){
	   Purpose: returns the width of each cell
       Parameters:  NONE 
	   Return Value: 
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public float getCellWidth(){
		return cWidth/mazeSize; 
	}

	/** 
	 * Function Name: getCellHeight(){
	   Purpose: returns the height of each cell
       Parameters:  NONE 
	   Return Value: 
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */		
	public float getCellHeight(){
		return cHeight/ mazeSize; 
	}

	/** 
	 * Function Name: createRandomNum(int val)
	   Purpose: returns a random number from 0 - val(param)
       Parameters:  int val -> the max range for the random number 
	   Return Value: integer value of random number generated
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public int createRandomNum(int val){
		Random rand = new Random(); 
		return rand.nextInt(val);
	}

	/** 
	 * Function Name: isWayThrough(int numOfChildren)
	   Purpose: decides if there is a way to go from the curCell 
       Parameters:  int numOfChildren -> num of Children to the curCell 
	   Return Value: returns 0 or 1 
	   Local Variables: Random rand -> random number generator

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public int isWayThrough(int numOfChildren){ 	
		Random rand = new Random();
		return 1/((rand.nextInt(numOfChildren))+1);
	}
	
	/** 
	 * Function Name: getInitCell()
	   Purpose: Cell that initiates the maze 
       Parameters:  None 
	   Return Value: None
	   Local Variables: None

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public Cell getInitCell(){
		return allCell[createRandomNum(mazeSize)]; 
	}

	public int getReverseCellNum(Cell aCell){
		int lVal = (int)aCell.yPos*mazeSize;
		return lVal + (int)aCell.xPos; 
	}
	
	/** 
	 * Function Name: getCellNum(Cell aCell)
	   Purpose: Given a cell computes the number allCell[number] in the list  
       Parameters:  Cell aCell -> input cell to compute the number 
	   Return Value: int -> number
	   Local Variables: int lVal -> xPos of Cell * mazeSize

Algorithm:
	   1) get xPos * mazeSize of the cell
	   2) add yPOs to lVal 
	   3) return the result

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public int getCellNum(Cell aCell){
		int lVal = (int)aCell.xPos*mazeSize;
		return lVal + (int)aCell.yPos; 
	}

/** 
	 * Function Name: getCellChild(Cell curCell)
	   Purpose: Given a cell returns all possible  children  
       Parameters:  Cell curCell -> input cell to find its children 
	   Return Value: Cell[] -> list of children
	   Local Variables: Cell temp -> creates an empty Cell
						Cell aCell -> creates a Cell with the childrens x/yPos
Algorithm:
	   1) Get the left right top and bottom children of the current cell
	   2) Check if a child cell is valid
	   3) if valid add to the childCell aCell (with updated x/yPOs) 
	   		else: add temp Cell (default)		

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public Cell[] getCellChild(Cell curCell){
		
		/*if (curCell.xPos == 0 ) {
			
		}*/
		
		
		Cell childCells[] = new Cell[4]; 
		Cell temp = new Cell(-1,-1); 
		Cell aCell = new Cell(-1,-1);// = new Cell(); 
		
		//Left
		aCell= new Cell(curCell.xPos-1, curCell.yPos); 
		if (getCellNum(aCell) > (mazeSize * mazeSize) || getCellNum(aCell) < 0 ||aCell.yPos >= mazeSize || aCell.xPos >= mazeSize ||aCell.yPos <0  || aCell.xPos <0 ) {
			childCells[0] = temp; 
		}
		else {
			childCells[0] = aCell;
		}
		
		//Right
		aCell= new Cell(curCell.xPos+1, curCell.yPos);
		if (getCellNum(aCell) > (mazeSize * mazeSize) || getCellNum(aCell) < 0 ||aCell.yPos >= mazeSize || aCell.xPos >= mazeSize ||aCell.yPos <0  || aCell.xPos <0 ) {
			childCells[1] = temp; 
		}
		else {
			childCells[1] = aCell;
		}
		
		//Top 
		aCell= new Cell(curCell.xPos, curCell.yPos+1); 
		if (getCellNum(aCell) > (mazeSize * mazeSize) || getCellNum(aCell) < 0 ||aCell.yPos >= mazeSize || aCell.xPos >= mazeSize ||aCell.yPos <0  || aCell.xPos <0 ) {

			childCells[2] = temp; 
		}
		else {
			childCells[2] = aCell;
		}
		
		//Bottom
		aCell= new Cell(curCell.xPos, curCell.yPos-1);
		if (getCellNum(aCell) > (mazeSize * mazeSize) || getCellNum(aCell) < 0 ||aCell.yPos >= mazeSize || aCell.xPos >= mazeSize ||aCell.yPos <0  || aCell.xPos <0 ) {
			childCells[3] = temp; 
		}
		else {
			childCells[3] = aCell;
		}
		return childCells; 
	}
	
/** 
	 * Function Name: updateCells(Cell aCell)
	   Purpose: Given a cell update its visited, children, isWay values 
       Parameters:  Cell curCell -> input cell to find update 
	   Return Value: void
	   Local Variables: int count -> counter possible children
						Cell lCell -> list of children cells
Algorithm:
	   1) make the curChild isVisited true; 
	   2) Check if the child Cell is valid -> increase count
	   3) update isWay based on the of curCell based on the isWay of its children
	   4) For remaining uninitialized isWay, compute the isWay function to determine.   
	   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
*/		
	public void updateCells(Cell aCell){
		aCell.isVisited = true;
		int count = 0; 
		Cell lCell[]=getCellChild(aCell);
		for (int i = 0 ; i < 4 ; i++) {
			
			if (lCell[i].xPos == -1) {
				continue; 
			}
			else {
				count++;
			}
			
		}
		for (int i = 0 ; i < 4 ; i++) {
			if (lCell[i].xPos == -1) {
				aCell.isWay[i] = 0; 
				continue; 
			}
			if (getCellNum(lCell[i]) >= 0 && getCellNum(lCell[i]) < mazeSize*mazeSize ) {
				if (allCell[getCellNum(lCell[i])].isVisited == true ) {
					//aCell.isWay[i] = 0;
					switch(i){
					case 0:
						aCell.isWay[i] = allCell[getCellNum(aCell) - mazeSize].isWay[1]; 
						break;
					case 1:
						aCell.isWay[i] = allCell[getCellNum(aCell) + mazeSize].isWay[0];
						break;
					case 2:
						aCell.isWay[i] = allCell[getCellNum(aCell) + 1].isWay[3];
						break;
					case 3:
						aCell.isWay[i] = allCell[getCellNum(aCell) - 1].isWay[2];
						break;
					}
					continue; 
				}
				else {
					aCell.isWay[i] = isWayThrough(count - 1);
					if (aCell.isWay[i] == 0 && count > 0) { 
						count -=1; 
					}
				}
			}
		}
	}

	/** 
	 * Function Name: initCells()
	   Purpose: Initialize allCell with a defaul xPos, and yPos in the screen 
       Parameters:  None
	   Return Value: None
	   Local Variables: int count -> curCell counter
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void initCells(){
		int count = 0;
		for (int i = 0 ; i < mazeSize; i++){
			for (int j = 0 ; j < mazeSize; j++){ 
				allCell[count++] = new Cell(i,j); 
			}
		}
	}
	
	public void reverseInitCells(){
		int count = 0;
		for (int i = 0 ; i < mazeSize; i++){
			for (int j = 0 ; j < mazeSize; j++){ 
				allCell[count++] = new Cell(j,i); 
			}
		}
	}
	

	/** 
	 * Function Name: retCells()
	   Purpose: Getter of allCells 
       Parameters:  None
	   Return Value: Cell[] allCell
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public Cell[] retCells(){
		return allCell; 
	}
	/** 
	 * Function Name: setCells(Cell aCell, int pos)
	   Purpose: Setter of a specific cell 
       Parameters: Cell aCell -> the cell to replace in the allCells
       				int pos -> position to replace aCell
	   Return Value: None
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */		
	public void setCells(Cell aCell, int pos){
		this.allCell[pos] = aCell; 
	}
	/** 
	 * Function Name: retCells(Cell aCell)
	   Purpose: return a particular cell from allCells
       Parameters: Cell aCell -> the cell to be returned from allCells
	   Return Value: The needed cell 
	   Local Variables: None
	   		
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public Cell retCell(Cell aCell){
		return allCell[getCellNum(aCell)];
	}
	public int retMazeSize(){
		return mazeSize; 
	}
	public void resetIsVisited() {
		for (int i = 0 ; i < mazeSize*mazeSize ; i++) {
			allCell[i].isVisited = false; 
		}
	}
	
	/*public void createTree1(){
		
	}*/
	
	
}
