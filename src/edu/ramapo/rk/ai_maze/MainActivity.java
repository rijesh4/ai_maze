 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/


package edu.ramapo.rk.ai_maze;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	//private Spinner spinner1;

	/** 
	 * Function Name: onCreate()
	   Purpose: created at creation of the activity -> Set up ViewContent (using the xml screen)
       Parameters:  Bundle savedInstanceState
	   Return Value: void
	   Local Variables: NONE
	   		 
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//addListenerOnSpinnerItemSelection();
		setContentView(R.layout.activity_main);
		TextView textView = (TextView)findViewById(R.id.enterNum);
		textView.setText("10");
	}
	
	/*public void addListenerOnSpinnerItemSelection() {
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
	  }*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	              default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	
	
/** 
	 * Function Name: createMaze
	   Purpose: Call back function for button click to start maze generation 
       Parameters:  View view
	   Return Value: void
	   Local Variables: int mazeSize -> accepts string (numVal) entered by user
	   					int duration -> toast notify timer
	   		
Algorithm: 
	1) Read user input 
	2) Check if <= 6 : notify
	2)  else start a new intent for new activity (The_Maze) 
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
public void createMaze (View view) {
		
		TextView textView = (TextView)findViewById(R.id.enterNum);
		Intent intent = new Intent (this, The_Maze.class);
		int mazeSize = Integer.parseInt(textView.getText().toString());
		if (mazeSize < 6) {
			CharSequence text = "Please enter a number greater than 5.";
			   int duration = 5000;

			   Toast toast = Toast.makeText(getApplicationContext(), text, duration);
			   toast.show();
		}
		else {
			intent.putExtra("maze_size", mazeSize); 
			intent.putExtra("isLoad", false); 
			startActivity(intent);
		}
		 		
	}
public void loadMaze (View view) {
		int mazeSize = 10; 
		Intent intent = new Intent (this, The_Maze.class);
		intent.putExtra("maze_size", mazeSize); 
		intent.putExtra("isLoad", true); 
		startActivity(intent);						 
	}
}
