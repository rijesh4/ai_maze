 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.ai_maze;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/** 
 * Class Name: The_Maze
   Purpose: It creates a new View class, after receiving the data from the user 
   Parameters:  
   Return Value: void
   Local Variables: int mazeSize -> accepts string (numVal) entered by user
   		
Algorithm: 
1) Accept data sent from getIntent
2) pass data to a newly created view. 
 
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
*/
public class The_Maze extends Activity {
	
	private int mazeSize; 
	private int pathCode = 0; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent(); 
		this.mazeSize = intent.getIntExtra("maze_size", 10);
		
		if (intent.getBooleanExtra("isLoad", true)) {
			View pathView = new PathView(this, mazeSize, 0, 0);
			setContentView(pathView);
		}
		else{
			View mazeView = new MazeView(this, mazeSize);
			setContentView(mazeView);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.the__maze, menu);
		return true;
	}
	
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
		int x = 0; 
	    switch (item.getItemId()) {
		    case R.id.bfsearch:
		    	menuCodeViewSet(1);
		    	return true; 
		    case R.id.dfsearch:
		    	menuCodeViewSet(2);
		    	return true; 
		    
		    case R.id.complexity:
		    	menuCodeViewSet(3);
		    	return true;
		    
		    case R.id.BestFirst:
		    	menuCodeViewSet(4);
		    	return true;
		    
		    case R.id.AStar:
		    	menuCodeViewSet(5);
		    	return true; 
		    
		    case R.id.aFile1:
		    	//pathCode = 6; 
		    	menuCodeViewSet(6);
		    	return true;
		    	
		    case R.id.aFile2:
		    	pathCode = 7; 
		    	menuCodeViewSet(7);
		    	return true;
		    
		    case R.id.aFile3:
		    	pathCode = 8; 
		    	menuCodeViewSet(8);
		    	return true;	
		  
		    default:
		        //menuCodeViewSet();
		        return true; 
	    }
	}
	/** 
	 * function Name:menuCodeViewSet
	   Purpose: gets the menuCode and calls the view screen 
	   Parameters: val : menuCode
	   Return Value: none
	   Local Variables: None
		 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void menuCodeViewSet(int val ){
		View mazeView = new PathView(this, mazeSize, val , pathCode);;
		setContentView(mazeView);
		  
	}
	
	 
}
