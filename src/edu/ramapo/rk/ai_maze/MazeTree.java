package edu.ramapo.rk.ai_maze;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MazeTree extends CreateMaze{
	
	/**
	 *  // Need Exit Node. 
	 */
	private static final long serialVersionUID = 991433785446092963L; 
	private Cell headNode; 
	private Cell[] listCells; 
	private ArrayList<Cell> exitCells = new ArrayList<Cell>(); 
	//private ArrayList<Cell> activeL = new ArrayList<Cell>(); 
	
	MazeTree(int mazeSize){
		this.mazeSize = mazeSize; 
		allCell =  new Cell[mazeSize*mazeSize];
		listCells = retCells();
	}
	
	/** 
	 * Function Name: getHeadNode
	   Purpose: finds the head node from the Given maze  
       Parameters:  None 
	   Return Value: Cell -> the head Node
	   Local Variables:none
Algorithm:
	   1) Traverse the vertical blocks of the maze.
	   2) Find the cell with isWay[0] = true

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	private Cell getHeadNode(){
		for (int i = 0 ; i < mazeSize ; i++) {
			if (listCells[i*mazeSize].isWay[0] == 1) {
				headNode = listCells[i*mazeSize];
				headNode.setParent(new Cell(-1,-1)); 
				return headNode; 
			}
		}
		return new Cell(-1,-1); 	
	}
	
	
	public ArrayList<Cell> retExitCells(){
		return exitCells; 
	}
	public Cell retHeadNode(){
		return headNode; 
	}
	
	/** 
	 * Function Name: isTurn
	   Purpose: finds if there is a turn  
       Parameters:  parentCell and childCell 
	   Return Value: boolean
	   Local Variables:none
Algorithm:
	   1) check if the direction of movement changes from grandparent -> parent -> child

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	private boolean isTurn(Cell parentCell, Cell childCell) {
			
		if (parentCell.getParent().xPos == -1) {
			if (childCell.isWay[0] == 1) {
				return false; 
			}
			if (childCell.isWay[2] == 1) {
				return true; 
			}
			if (childCell.isWay[3] == 1) {
				return true; 
			}
			if (childCell.isWay[1] == 1) {
				return false; 
			}
		}
			
		if (parentCell.getBChild() == childCell && parentCell.getParent().getBChild() == parentCell) {
			return false; 
		}
		if (parentCell.getTChild() == childCell && parentCell.getParent().getTChild() == parentCell) {
			return false; 
		}
		if (parentCell.getLChild() == childCell && parentCell.getParent().getLChild() == parentCell) {
			return false; 
		}
		if (parentCell.getRChild() == childCell && parentCell.getParent().getRChild() == parentCell) {
			return false; 
		}
		
		return true; 
	}

	/** 
	 * Function Name: getPathtoStart
	   Purpose: given a node finds the list of parents until the headNode  
       Parameters:  Cell : curNode 
	   Return Value: List of Cells 
	   Local Variables:PathList (ArrayList) to be returned
Algorithm:
	   1) In a loop find the parent until headNode is reached
	   2) add to list

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	
	public ArrayList<Cell> getPathtoStart (Cell curNode){
		ArrayList <Cell> PathList = new ArrayList<Cell>(); 
		PathList.add(curNode);
		Cell curCell = curNode; 
		
		while (curCell.getParent().xPos != -1) {
			curCell = curCell.getParent(); 
			PathList.add(curCell);
		}
		return PathList; 
	}
	
	/**  
	 * Function Name: manhattanDistance
	   Purpose: finds average manhattan distance from a Node  
       Parameters: Cell : curNode
	   Return Value: none
	   Local Variables: int: distance, length, width
Algorithm:
	   1) For each exitCells find the length and width from the node
	   2) save in distance and set the value for the node

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void manhattanDistance(Cell curNode){
		float distance = 0;
		float length = 0; 
		float width = 0; 
		for(int i = 0 ; i < exitCells.size(); i++){
			Cell aCell = exitCells.get(i);
			length = aCell.xPos - curNode.xPos; 
			width = Math.abs(aCell.yPos - curNode.yPos); 
			distance += length += width;  
		}
		curNode.heuristicVal = distance / exitCells.size(); 
	}
	
	/** 
	 * Function Name: AStarSearch
	   Purpose: find the path traversal using A* and BestFirstSearch  
       Parameters:  boolean: noDepth //  Distinguishes A* from BestFirstSearch 
	   Return Value: List of Cell
	   Local Variables:none
Algorithm:
	   1) In activeList add the headNOde
	   2) in a loop:
	   		i) pop cell from list
	   		ii) populate the list with curCell's child
	   		iii) sort according to heuristic value respectively 

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public ArrayList<Cell> AstarSearch(boolean noDepth){
		branchAndBound(); 
		ArrayList<Cell> activeList = new ArrayList<Cell>(); 
		ArrayList<Cell> visitedList = new ArrayList<Cell>();
		activeList.add(headNode);
		
		while (!activeList.isEmpty()) {
			Cell curNode = activeList.remove(0);
			visitedList.add(curNode); 
 
			if (curNode.getLChild().xPos != -1) {
				activeList.add(curNode.getLChild());
			}
			if (curNode.getRChild().xPos != -1) {
				activeList.add(curNode.getRChild()); 
			}
			if (curNode.getTChild().xPos != -1) {
				activeList.add(curNode.getTChild()); 
			}
			if (curNode.getBChild().xPos != -1) {
				activeList.add(curNode.getBChild());
			}
			
			if (!noDepth) {
				Collections.sort(activeList, new HeurComp());
			}
			else {
				Collections.sort(activeList, new NoDepthComp());
			}
			
		}
		return visitedList; 
	}
	
	/** 
	 * Function Name: getComplexity
	   Purpose: finds complexity of a cell  
       Parameters:  Cell: curNOde
	   Return Value: Int: complexity value
	   Local Variables:int : length , turn
Algorithm:
	   1) find the depth by reaching onto parent
	   2) find turn by calling isTurn function

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public int getComplexity (Cell curNode) {
		int length = 0;
		int turn = 0; 
		Cell theNode = curNode; 
		while (curNode.getParent().xPos != -1) {
			if (isTurn(curNode.getParent(), curNode)){ 
				turn++ ;
			} 
			curNode = curNode.getParent();
			length++;  
		}
		theNode.depth = length; 
		/*System.out.println(theNode.xPos + "," + theNode.yPos);
		System.out.println(length +" "+ turn);*/
		return length * turn; 
	}
	
	public int getNewComplexity (Cell curNode) {
		int length = 0;
		int turn = 0; 
		Cell theNode = curNode; 
		while (curNode.getParent().xPos != -1) {
			if (isTurn(curNode.getParent(), curNode)){ 
				turn++ ;
			} 
			curNode = curNode.getParent();
			length++;  
		}
		theNode.depth = length; 
		System.out.println("("+ theNode.xPos + "," + theNode.yPos +")");
		System.out.println("length: " +length +" turns: "+ turn + "complexity:  " + length * turn );
		return length * turn; 
	}
	
	/** 
	 * Function Name: branchAandBound
	   Purpose: finds the complexity of the whole maze  
       Parameters:  None 
	   Return Value: Float
	   Local Variables:ActiveList: holds the curCells being considered
Algorithm:
	   1) traverse the whole tree, until null/ emptyList
	   2) for each path find the ones with deadEnd
	   3) add the value of the deadEnd and ExitCells complexity, and return

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public float branchAndBound() {
		ArrayList<Cell> activeList = new ArrayList<Cell>(); 
		activeList.add(headNode);
		headNode.complexityVal = 0; 
		float totalComplexity = 0; 
		headNode.depth = 0; 
		
		while (!activeList.isEmpty()) {
			Cell curNode = activeList.remove(0);
			manhattanDistance(curNode);
			int childCount = 0; 
			if (curNode.getLChild().xPos != -1) {
				curNode.getLChild().complexityVal = getComplexity(curNode.getLChild()); 
				activeList.add(curNode.getLChild());
				childCount++; 
			}
			if (curNode.getRChild().xPos != -1) {
				curNode.getRChild().complexityVal = getComplexity(curNode.getRChild());
				activeList.add(curNode.getRChild()); 
				childCount++;
			}
			if (curNode.getTChild().xPos != -1) {
				curNode.getTChild().complexityVal = getComplexity(curNode.getTChild());
				activeList.add(curNode.getTChild()); 
				childCount++;
			}
			if (curNode.getBChild().xPos != -1) {
				curNode.getBChild().complexityVal = getComplexity(curNode.getBChild());
				activeList.add(curNode.getBChild()); 
				childCount++;
			}
			if (childCount == 0) {
				totalComplexity += curNode.complexityVal;
				getNewComplexity(curNode);
				//System.out.println(curNode.xPos + "," + curNode.yPos);
				//System.out.println(curNode.complexityVal);
				
			}
			Collections.sort(activeList, new CellComp());
						
		}
		return totalComplexity;  
	}
	
	/** 
	 * Function Name: createTree
	   Purpose: creates a Tree from the maze  
       Parameters:  None 
	   Return Value: none
	   Local Variables: ArrayList
Algorithm:
	   1) Uses modified BFS to find the all available cells and their respective children 
	   2) update the value of the Cells accordingly
	   3) if exit node found add to exitCell List

 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void createTree(){
		ArrayList <Cell> BFSList = new ArrayList<Cell>(); 
		BFSList.add(getHeadNode()); 
		resetIsVisited(); 	
		
		while (!BFSList.isEmpty()) {
			Cell curNode = BFSList.remove(0); 
			curNode.isVisited= true; 
			Cell listChild[] = getCellChild(curNode);
			 
			Cell aCell = listChild[0];
			if (aCell.xPos != -1 ) {
				aCell =allCell[getReverseCellNum(listChild[0])];
				if (curNode.isWay[0] == 1 && !aCell.isVisited &&aCell.xPos != -1) { 
					curNode.setLChild(aCell); 
					aCell.setParent(curNode); 
					BFSList.add(aCell); 
				}
				else {
					curNode.setLChild(new Cell(-1,-1)); 
				}
			}
			else {
				curNode.setLChild(new Cell(-1,-1)); 
			}
			
			aCell = listChild[1];
			if (aCell.xPos != -1) {
				aCell =allCell[getReverseCellNum(listChild[1])];
				if (curNode.isWay[1] == 1 && !aCell.isVisited &&aCell.xPos != -1) { 
					curNode.setRChild(aCell); 
					aCell.setParent(curNode); 
					BFSList.add(aCell); 
				}
				else {
					curNode.setRChild(new Cell(-1,-1));  
				}
			}
			else {
				curNode.setRChild(new Cell(-1,-1)); 
			}
			
			aCell = listChild[3];
			if (aCell.xPos != -1) {
				aCell =allCell[getReverseCellNum(listChild[3])];
				if (curNode.isWay[2] == 1 && !aCell.isVisited &&aCell.xPos != -1) { 
					curNode.setTChild(aCell); 
					aCell.setParent(curNode); 
					BFSList.add(aCell); 
				}
				else {
					curNode.setTChild(new Cell(-1,-1)); 
				}
			}
			else {
				curNode.setTChild(new Cell(-1,-1));  
			}
			
			aCell = listChild[2];
			if (aCell.xPos != -1) {
				aCell =allCell[getReverseCellNum(listChild[2])];
				if (curNode.isWay[3] == 1 && !aCell.isVisited &&aCell.xPos != -1) { 
					curNode.setBChild(aCell); 
					aCell.setParent(curNode); 
					BFSList.add(aCell); 
				}
				else {
					curNode.setBChild(new Cell(-1,-1));  
				}
			}
			else {
				curNode.setBChild(new Cell(-1,-1)); 
			}
			setCells(curNode, getReverseCellNum(curNode)); 
			if (curNode.xPos == mazeSize -1 && curNode.isWay[1] == 1) {
				exitCells.add(curNode);  
			}
		}	
	}
}

/** 
 * class Name: CellComparator
   Purpose: Compares complexity of 2 cells  
   Parameters:  None 
   Return Value: boolean
   Local Variables:none
Algorithm:
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
*/

class CellComp implements Comparator<Cell>{
	 
    @Override
    public int compare(Cell c1, Cell c2) {	
        if(c1.complexityVal < c2.complexityVal){
            return 1;
        } else {
            return -1;
        }
    }
}

/** 
 * class Name: HeurComparator
   Purpose: Compares Heuristic+Depth of 2 cells for A*  
   Parameters:  None 
   Return Value: boolean
   Local Variables:none
Algorithm:
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
*/
class HeurComp implements Comparator<Cell>{
	 
    @Override
    public int compare(Cell c1, Cell c2) {	
        if((c1.heuristicVal +  c1.depth  ) > (c2.heuristicVal + c2.depth )){
            return 1;
        } else {
            return -1;
        }
    }
}

/** 
 * class Name: NoDepthComparator
   Purpose: Compares Heuristics of 2 cells  
   Parameters:  None 
   Return Value: boolean
   Local Variables:none
Algorithm:
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
*/
class NoDepthComp implements Comparator<Cell>{
	 
    @Override
    public int compare(Cell c1, Cell c2) {	
        if(c1.heuristicVal  > c2.heuristicVal ){
            return 1;
        } else {
            return -1;
        }
    }
}


